builds `docker` image with  docker-compose installed

Use this in your `.gitlab-ci.yml`:

```yaml
image: registry.gitlab.com/passit/docker-compose
services:
  - docker:dind

...
```

rebuilt every day from `docker:latest`